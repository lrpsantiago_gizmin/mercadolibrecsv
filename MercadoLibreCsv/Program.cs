﻿using MercadoLibreCsv.Models;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MercadoLibreCsv
{
    class Program
    {
        private static readonly string acessToken = "APP_USR-1834929205100904-082614-2ae6fd6c30b2bf726d628fceda90276a-446655208";

        private static void Main(string[] args)
        {
            try
            {
                var api = InitizlizeApi();
                var response = RequestApiData(api);

                GenerateCsvFile(response);

                Console.WriteLine("\nPrograma finalizado com sucesso, pressione qualquer tecla para sair.");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                var prevColor = Console.ForegroundColor;

                WriteLineColor("ERRO", ConsoleColor.Red);
                WriteLineColor($"[Causa: {e.Message}]", ConsoleColor.White);

                Console.WriteLine("\nPrograma finalizado, pressione qualquer tecla para sair.");
                Console.ReadKey();
            }
        }

        private static ApiRest InitizlizeApi()
        {
            Console.Write("Iniciando Cliente.....");

            var api = new ApiRest("https://api.mercadopago.com");

            WriteLineColor("OK", ConsoleColor.Green);
            return api;
        }

        private static Response RequestApiData(ApiRest api)
        {
            Console.Write("Realizando Consulta...");

            var resp = api.GetAsync<Response>($"/v1/payments/search?access_token={acessToken}")
                .Result;

            WriteLineColor("OK", ConsoleColor.Green);
            return resp;
        }

        private static void GenerateCsvFile(Response response)
        {
            Console.Write("Gerando Arquivo.......");
            CsvConfig.ItemSeperatorString = ";";

            var serializer = new Helper.CsvSerializer();
            var csvContent = serializer.SerializeToCsv(response.Results);
            var desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var filePath = Path.Combine(desktop, "MercadoLibre.csv");

            File.WriteAllText(filePath, csvContent);

            WriteLineColor("OK", ConsoleColor.Green);

            WriteLineColor($"[Arquivo gerado em: {filePath}]", ConsoleColor.White);
        }

        private static void WriteLineColor(string text, ConsoleColor color)
        {
            var prevColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = prevColor;
        }
    }
}
