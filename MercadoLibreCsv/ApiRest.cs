﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MercadoLibreCsv
{
    public class ApiRest
    {
        private readonly HttpClient _client;
        private readonly JsonSerializerSettings _serilizerSettings;

        public ApiRest(string baseUrl, IDictionary<string, string> headerItems = null)
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(baseUrl)
            };

            if (headerItems != null)
            {
                _client.DefaultRequestHeaders
                    .Clear();

                foreach (var item in headerItems)
                {
                    _client.DefaultRequestHeaders.TryAddWithoutValidation(item.Key, item.Value);
                }
            }

            _serilizerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }

        public async Task<T> GetAsync<T>(string route)
        {
            var response = await _client.GetAsync(route);
            await ValidateResponse(response);

            return await UnpackContentAsync<T>(response);
        }

        public async Task<T> PostAsync<T>(string route, object obj)
        {
            var content = PackContent(obj);
            var response = await _client.PostAsync(route, content);

            await ValidateResponse(response);

            return await UnpackContentAsync<T>(response);
        }

        public async Task<T> PutAsync<T>(string route, object obj)
        {
            var content = PackContent(obj);
            var response = await _client.PutAsync(route, content);

            await ValidateResponse(response);

            return await UnpackContentAsync<T>(response);
        }

        public async Task<T> DeleteAsync<T>(string route)
        {
            var response = await _client.DeleteAsync(route);

            await ValidateResponse(response);

            return await UnpackContentAsync<T>(response);
        }

        public async Task PostAsync(string route, object obj)
        {
            await PostAsync<object>(route, obj);
        }

        public async Task PutAsync(string route, object obj)
        {
            await PutAsync<object>(route, obj);
        }

        public async Task DeleteAsync(string route, object obj)
        {
            await PutAsync<object>(route, obj);
        }

        private HttpContent PackContent(object obj)
        {
            if (obj == null)
            {
                return new StringContent(string.Empty);
            }

            var serializedObj = JsonConvert.SerializeObject(obj, _serilizerSettings);
            var content = new StringContent(serializedObj, Encoding.UTF8, "application/json");

            return content;
        }

        private async Task<T> UnpackContentAsync<T>(HttpResponseMessage response)
        {
            var strResponseContent = await response.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<T>(strResponseContent);

            return responseObj;
        }

        private async Task ValidateResponse(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                var strResponseContent = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrWhiteSpace(strResponseContent))
                {
                    throw new Exception($"({(int)response.StatusCode}) - {response.ReasonPhrase}");
                }

                var responseObj = JsonConvert.DeserializeObject<ErrorResponse>(strResponseContent);

                throw new Exception(responseObj.Message);
            }
        }
    }
}
