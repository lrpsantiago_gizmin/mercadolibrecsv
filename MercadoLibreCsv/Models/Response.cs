﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MercadoLibreCsv.Models
{
    public class Paging
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonProperty("offset")]
        public int Offset { get; set; }
    }

    public class Metadata
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("customer_id")]
        public string CustomerId { get; set; }
    }

    public class FeeDetail
    {
        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("fee_payer")]
        public string FeePayer { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class Identification
    {
        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class Phone
    {
        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("extension")]
        public object Extension { get; set; }

        [JsonProperty("area_code")]
        public string AreaCode { get; set; }
    }

    public class Payer
    {
        [JsonProperty("entity_type")]
        public object EntityType { get; set; }

        [JsonProperty("identification")]
        public Identification Identification { get; set; }

        [JsonProperty("phone")]
        public Phone Phone { get; set; }

        [JsonProperty("operator_id")]
        public object OperatorId { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }

    public class TransactionDetails
    {
        [JsonProperty("total_paid_amount")]
        public double TotalPaidAmount { get; set; }

        [JsonProperty("acquirer_reference")]
        public object AcquirerReference { get; set; }

        [JsonProperty("installment_amount")]
        public double InstallmentAmount { get; set; }

        [JsonProperty("financial_institution")]
        public object FinancialInstitution { get; set; }

        [JsonProperty("net_received_amount")]
        public double NetReceivedAmount { get; set; }

        [JsonProperty("overpaid_amount")]
        public int OverpaidAmount { get; set; }

        [JsonProperty("external_resource_url")]
        public object ExternalResourceUrl { get; set; }

        [JsonProperty("payable_deferral_period")]
        public object PayableDeferralPeriod { get; set; }

        [JsonProperty("payment_method_reference_id")]
        public string PaymentMethodReferenceId { get; set; }
    }

    public class Order
    {
    }

    public class Item
    {
        [JsonProperty("quantity")]
        public string Quantity { get; set; }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("picture_url")]
        public string PictureUrl { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("unit_price")]
        public string UnitPrice { get; set; }
    }

    public class ReceiverAddress
    {
        [JsonProperty("floor")]
        public string Floor { get; set; }

        [JsonProperty("apartment")]
        public string Apartment { get; set; }

        [JsonProperty("street_name")]
        public string StreetName { get; set; }

        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }
    }

    public class Shipments
    {
        [JsonProperty("receiver_address")]
        public ReceiverAddress ReceiverAddress { get; set; }
    }

    public class AdditionalInfo
    {
        [JsonProperty("items")]
        public IEnumerable<Item> Items { get; set; }

        [JsonProperty("payer")]
        public Payer Payer { get; set; }

        [JsonProperty("shipments")]
        public Shipments Shipments { get; set; }
    }

    public class Cardholder
    {
        [JsonProperty("identification")]
        public Identification Identification { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Card
    {
        [JsonProperty("first_six_digits")]
        public string FirstSixDigits { get; set; }

        [JsonProperty("expiration_year")]
        public int ExpirationYear { get; set; }

        [JsonProperty("date_created")]
        public DateTime DateCreated { get; set; }

        [JsonProperty("expiration_month")]
        public int ExpirationMonth { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("cardholder")]
        public Cardholder Cardholder { get; set; }

        [JsonProperty("last_four_digits")]
        public string LastFourDigits { get; set; }

        [JsonProperty("date_last_updated")]
        public DateTime DateLastUpdated { get; set; }
    }

    public class Source
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class Refund
    {
        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("date_created")]
        public DateTime DateCreated { get; set; }

        [JsonProperty("source")]
        public Source Source { get; set; }

        [JsonProperty("unique_sequence_number")]
        public string UniqueSequenceNumber { get; set; }

        [JsonProperty("payment_id")]
        public object PaymentId { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public class Result
    {
        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("corporation_id")]
        public object CorporationId { get; set; }

        [JsonProperty("operation_type")]
        public string OperationType { get; set; }

        [JsonProperty("fee_details")]
        public IEnumerable<FeeDetail> FeeDetails { get; set; }

        [JsonProperty("notification_url")]
        public string NotificationUrl { get; set; }

        [JsonProperty("date_approved")]
        public DateTime? DateApproved { get; set; }

        [JsonProperty("acquirer")]
        public object Acquirer { get; set; }

        [JsonProperty("money_release_schema")]
        public object MoneyReleaseSchema { get; set; }

        [JsonProperty("payer")]
        public Payer Payer { get; set; }

        [JsonProperty("transaction_details")]
        public TransactionDetails TransactionDetails { get; set; }

        [JsonProperty("statement_descriptor")]
        public string StatementDescriptor { get; set; }

        [JsonProperty("call_for_authorize_id")]
        public object CallForAuthorizeId { get; set; }

        [JsonProperty("installments")]
        public int Installments { get; set; }

        [JsonProperty("pos_id")]
        public object PosId { get; set; }

        [JsonProperty("external_reference")]
        public string ExternalReference { get; set; }

        [JsonProperty("date_of_expiration")]
        public object DateOfExpiration { get; set; }

        [JsonProperty("id")]
        public object Id { get; set; }

        [JsonProperty("payment_type_id")]
        public string PaymentTypeId { get; set; }

        [JsonProperty("order")]
        public Order Order { get; set; }

        [JsonProperty("counter_currency")]
        public object CounterCurrency { get; set; }

        [JsonProperty("status_detail")]
        public string StatusDetail { get; set; }

        [JsonProperty("differential_pricing_id")]
        public object DifferentialPricingId { get; set; }

        [JsonProperty("additional_info")]
        public AdditionalInfo AdditionalInfo { get; set; }

        [JsonProperty("live_mode")]
        public bool LiveMode { get; set; }

        [JsonProperty("marketplace_owner")]
        public object MarketplaceOwner { get; set; }

        [JsonProperty("card")]
        public Card Card { get; set; }

        [JsonProperty("integrator_id")]
        public object IntegratorId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("transaction_amount_refunded")]
        public double TransactionAmountRefunded { get; set; }

        [JsonProperty("transaction_amount")]
        public double TransactionAmount { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("money_release_date")]
        public DateTime? MoneyReleaseDate { get; set; }

        [JsonProperty("merchant_number")]
        public object MerchantNumber { get; set; }

        [JsonProperty("refunds")]
        public IEnumerable<Refund> Refunds { get; set; }

        [JsonProperty("authorization_code")]
        public string AuthorizationCode { get; set; }

        [JsonProperty("captured")]
        public bool Captured { get; set; }

        [JsonProperty("collector_id")]
        public int CollectorId { get; set; }

        [JsonProperty("merchant_account_id")]
        public object MerchantAccountId { get; set; }

        [JsonProperty("taxes_amount")]
        public int TaxesAmount { get; set; }

        [JsonProperty("date_last_updated")]
        public DateTime DateLastUpdated { get; set; }

        [JsonProperty("coupon_amount")]
        public int CouponAmount { get; set; }

        [JsonProperty("store_id")]
        public object StoreId { get; set; }

        [JsonProperty("date_created")]
        public DateTime DateCreated { get; set; }

        [JsonProperty("acquirer_reconciliation")]
        public IEnumerable<object> AcquirerReconciliation { get; set; }

        [JsonProperty("sponsor_id")]
        public object SponsorId { get; set; }

        [JsonProperty("shipping_amount")]
        public int ShippingAmount { get; set; }

        [JsonProperty("issuer_id")]
        public string IssuerId { get; set; }

        [JsonProperty("payment_method_id")]
        public string PaymentMethodId { get; set; }

        [JsonProperty("binary_mode")]
        public bool BinaryMode { get; set; }

        [JsonProperty("platform_id")]
        public object PlatformId { get; set; }

        [JsonProperty("deduction_schema")]
        public object DeductionSchema { get; set; }

        [JsonProperty("processing_mode")]
        public string ProcessingMode { get; set; }

        [JsonProperty("currency_id")]
        public string CurrencyId { get; set; }

        [JsonProperty("shipping_cost")]
        public int ShippingCost { get; set; }
    }

    public class Response
    {
        [JsonProperty("paging")]
        public Paging Paging { get; set; }

        [JsonProperty("results")]
        public IEnumerable<Result> Results { get; set; }
    }
}