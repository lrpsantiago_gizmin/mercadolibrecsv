﻿using System.Collections.Generic;

namespace MercadoLibreCsv
{
    internal class ErrorResponse
    {
        public string Message { get; set; }

        public IEnumerable<string> Cause { get; set; }

        public string Error { get; set; }

        public int Status { get; set; }
    }
}