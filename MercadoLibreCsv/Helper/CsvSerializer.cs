﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MercadoLibreCsv.Helper
{
    public class CsvSerializer
    {
        public string SerializeToCsv(IEnumerable<object> objects)
        {
            var headers = GenerateHeaders(objects);
            return string.Join(";", headers);
        }

        private void RecursiveSerialization(object obj, Dictionary<string, object> dictionary, string prefix)
        {
            if (obj == null)
            {
                return;
            }

            var type = obj.GetType();

            if (type.IsSystemType())
            {
                dictionary.Add(prefix, obj);
                return;
            }

            foreach (var prop in type.GetProperties())
            {
                var key = prop.Name;
                RecursiveSerialization(prop.GetValue(obj), dictionary, $"{prefix}/{key}");
            }
        }

        private IEnumerable<string> GenerateHeaders(IEnumerable<object> objects)
        {
            var obj = objects.First();
            var rootType = obj.GetType();
            var headers = new List<string>();
            var properties = rootType.GetProperties();

            foreach (var prop in properties)
            {
                var key = prop.Name;
                var propType = prop.GetType();

                if (!propType.IsUserType())
                {
                    headers.Add(key);
                }
                else
                {
                    RecursiveHeaderGeneration(propType, headers, key);
                }
            }

            return headers;
        }

        private void RecursiveHeaderGeneration(Type type, IList<string> headers, string prefix)
        {
            foreach (var prop in type.GetProperties())
            {
                var key = prop.Name;

                if (!type.IsUserType())
                {
                    headers.Add($"{prefix}/{key}");
                }
                else
                {
                    RecursiveHeaderGeneration(prop.GetType(), headers, $"{prefix}/{key}");
                }
            }
        }

        //public string GetCSVString(IEnumerable<dynamic> inputList)
        //{
        //    var outputList = new List<Dictionary<string, object>>();

        //    foreach (var item in inputList)
        //    {
        //        Dictionary<string, object> outputItem = new Dictionary<string, object>();
        //        RecursiveSerialization(item, outputItem, "");

        //        outputList.Add(outputItem);
        //    }

        //    var headers = outputList.SelectMany(t => t.Keys)
        //        .Distinct()
        //        .ToList();

        //    string csvString = ";" + string.Join(";", headers.ToArray()) + "\r\n";

        //    foreach (var item in outputList)
        //    {
        //        foreach (string header in headers)
        //        {
        //            if (item.ContainsKey(header) && item[header] != null)
        //                csvString = csvString + ";" + item[header].ToString();
        //            else
        //                csvString = csvString + ";";
        //        }

        //        csvString = csvString + "\r\n";
        //    }

        //    return csvString;
        //}

        //private void Flatten(dynamic item, Dictionary<string, object> outputItem, string prefix)
        //{
        //    if (item == null)
        //    {
        //        return;
        //    }

        //    foreach (var propertyInfo in item.GetType().GetProperties())
        //    {
        //        if (!propertyInfo.PropertyType.Name.Contains("AnonymousType"))
        //        {
        //            outputItem.Add(prefix + "/" + propertyInfo.Name, propertyInfo.GetValue(item));
        //        }
        //        else
        //        {
        //            Flatten(propertyInfo.GetValue(item), outputItem, (prefix.Equals("") ? propertyInfo.Name : prefix + "__" + propertyInfo.Name));
        //        }
        //    }
        //}
    }
}
